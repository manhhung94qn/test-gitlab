import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import * as _moment from 'moment';

import { animate, state, style, transition, trigger } from '@angular/animations';

// tslint:disable-next-line:no-duplicate-imports
import {defaultFormat as _rollupMoment, Moment} from 'moment';
import { ProjectReport } from '../_models/project-report';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ProjectReportService } from '../_services/projects-report.service';

const moment = _moment;

export const MONTH_ONLY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM',
  },
  display: {
    dateInput: 'YYYY/MM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-report-project',
  templateUrl: './report-project.component.html',
  styleUrls: ['./report-project.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*', display: 'flex' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MONTH_ONLY_FORMATS},
  ],
})
export class ReportProjectComponent implements OnInit {
  projectStatus = [];
  reportStatusValue = '';

  reportMonth = new FormControl(moment());
  reportYear = new FormControl(moment().year());
  reportByValue = 'm';
  reportBy: any[] = [{ value: 'm', text: 'Month'}, { value: 'y', text: 'Year'}];
  isReported = false;

  dataSource: MatTableDataSource<any>;
  displayedColumns = [
  'CustName',
  'ProjectName',
  'ProjectDate',
  'ProjectEstimateCost',
  'ProjectDeliveryDate',
  'ProjectPaymentDate',
  'ProjectStatus',
  'ProjectTotalEstimateCost'];
  expandedElement: any;
  isExpansionDetailRow = (i: number, row: any) => row.hasOwnProperty('detailRow');
  isLastRow = (data, index) => index === this.dataSource.data.length;

  constructor(private projectReportService: ProjectReportService) { }

  ngOnInit() {
    this.projectReportService.getMstByName('MstProjectStatus').subscribe(resp => {
      this.projectStatus = resp.mst;
    });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.reportMonth.value;
    ctrlValue.year(normalizedYear.year());
    this.reportMonth.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.reportMonth.value;
    ctrlValue.month(normalizedMonth.month());
    this.reportMonth.setValue(ctrlValue);
    datepicker.close();
  }

  loadProject() {
    this.isReported = true;
    // tslint:disable-next-line:max-line-length
    this.projectReportService.getProjectReports(this.reportByValue, this.reportMonth.value, this.reportYear.value, this.reportStatusValue).subscribe(resp => {
      const rows = [];
      resp.forEach(element => rows.push(element, { detailRow: true, element }));
      this.dataSource = new MatTableDataSource(rows);
    });
  }

  convertStyleObject(style: string) {
    return JSON.parse(style);
  }
}
