import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './report.component';
import { ReportProjectComponent } from './report-project/report-project.component';
import { ReportCustomersComponent } from './report-customers/report-customers.component';

const routes: Routes = [
  {
    path: '',
    component: ReportComponent,
    children: [
      {
        path: '',
        redirectTo: 'project'
      },
      {
        path: 'project',
        component: ReportProjectComponent
      },
      {
        path: 'customer',
        component: ReportCustomersComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
