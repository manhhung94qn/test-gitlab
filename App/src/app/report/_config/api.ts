import { InjectionToken } from '@angular/core';
import { API_DOMAIN } from 'src/app/_configs/api';

export const API = {
    // GasStation
    ProjectReport: {
        Report: API_DOMAIN + '/api/Project/Report',
        ReportStatistics: API_DOMAIN + '/api/Project/ReportStatistics',
        ReportStatisticsByCust: API_DOMAIN + '/api/Project/ReportStatisticsByCust',
    }
};
