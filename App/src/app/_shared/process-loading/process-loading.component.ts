import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataShareService } from '../Data-share/data-share.service';

@Component({
  selector: 'app-process-loading',
  templateUrl: './process-loading.component.html',
  styleUrls: ['./process-loading.component.scss']
})
export class ProcessLoadingComponent implements OnInit {

  constructor(private shareData: DataShareService) { }
  loadingFinishedOB: Subscription;
  loadingFinished: number = 0;
  ngOnInit() {
  }

}
